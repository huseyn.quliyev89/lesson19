package az.ingress.redishashcaching.model;

import lombok.*;
import lombok.experimental.FieldDefaults;
import org.springframework.data.annotation.Id;
import org.springframework.data.redis.core.RedisHash;
import org.springframework.data.redis.core.index.Indexed;

import java.io.Serial;
import java.io.Serializable;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@FieldDefaults(level = AccessLevel.PRIVATE)
@RedisHash("Student")
public class Student implements Serializable {
    @Serial
    private static final long serialVersionUID = 1L;
    public enum Gender {
        MALE, FEMALE
    }
    @Id
    @Indexed //Used to speed up the property based search
    private Integer id;
    @Indexed
    private String name;
    private String surname;
    private Gender gender;
    private int age;

}
