package az.ingress.redishashcaching.service;

import az.ingress.redishashcaching.model.Student;
import az.ingress.redishashcaching.repository.StudentRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class StudentServiceImpl implements StudentService{

    private final StudentRepository studentRepository;
    @Override
    public Student saveStudent(Student student) {
        return studentRepository.saveStudent(student);
    }

    @Override
    public Student updateStudent(Integer id, Student student) {
        return studentRepository.updateStudent(id,student);
    }

    @Override
    public void deleteStudent(Integer id) {
      studentRepository.deleteStudent(id);
    }

    @Override
    public Student findById(Integer id) {
        return studentRepository.findById(id);
    }

    @Override
    public List<Object> findAll() {
        return studentRepository.findAll();
    }
}
