package az.ingress.redishashcaching.service;

import az.ingress.redishashcaching.model.Student;

import java.util.List;

public interface StudentService {
    Student saveStudent(Student student);
    Student updateStudent(Integer id, Student student);
    void deleteStudent(Integer id);
    Student findById(Integer id);
    List<Object> findAll();
}
